//
//  Product.swift
//  WinFox
//
//  Created by Кирилл Тарасов on 28.10.2021.
//

import Foundation

struct Product: Codable {
    let image: String
    let price: Int
    let name: String
    let weight: Int
    let id: String
    let desc: String
    
}
