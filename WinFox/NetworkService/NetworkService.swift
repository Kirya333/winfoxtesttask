//
//  NetworkService.swift
//  WinFox
//
//  Created by Кирилл Тарасов on 28.10.2021.
//

import Foundation

enum ObtainProducts {
    case success(products: [Product])
    case failure(error: Error)
}

class NetworkService {
    let sessionConfiguration = URLSessionConfiguration.default
    let session = URLSession.shared
    let decoder = JSONDecoder()

    func obtainPosts(completion: @escaping (ObtainProducts) -> Void) {

        guard let url = URL(string: "http://94.127.67.113:8099/getGoods") else {
            return
        }

        session.dataTask(with: url) { [weak self] (data, response, error) in

            var result: ObtainProducts

            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }

            guard let strongSelf = self else {
                result = .success(products: [])
                return
            }

            if error == nil, let parseData = data {
                guard let products = try? strongSelf.decoder.decode([Product].self, from: parseData)
                else {
                    result = .success(products: [])
                    return
                }

                result = .success(products: products)
            } else {
                result = .failure(error: error!)
            }

        }.resume()

    }
    
}
