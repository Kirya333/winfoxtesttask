//
//  ProductRealm.swift
//  WinFox
//
//  Created by Кирилл Тарасов on 28.10.2021.
//

import Foundation
import RealmSwift

class ProductRealm: Object {
    @objc dynamic var image: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var price: Int = 0
    @objc dynamic var desc: String = ""
    @objc dynamic var weight = 0
    @objc dynamic var id: String = ""
    @objc dynamic var count: Int = 0

    
    convenience init(product: Product) {
        self.init()
        self.image = product.image
        self.price = product.price
        self.name = product.name
        self.weight = product.weight
        self.id = product.id
        self.desc = product.desc
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
