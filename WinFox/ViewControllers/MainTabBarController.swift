//
//  MainTabBarController.swift
//  WinFox
//
//  Created by Кирилл Тарасов on 28.10.2021.
//

import Foundation
import UIKit


class MainTabBarController: UITabBarController {
    
//MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let prodVC = storyboard.instantiateViewController(identifier: "productsVC") as! ProductsViewController
        let shoppingCartVC = storyboard.instantiateViewController(identifier: "shoppingCartVC") as! ShoppingCartViewController
        
        let productsViewController = generateNavController(vc: prodVC, title: "Products", image: .checkmark)
        let shoppingCartViewController = generateNavController(vc: shoppingCartVC, title: "Shopping Cart", image: .checkmark)
        
        self.viewControllers = [productsViewController, shoppingCartViewController]
        
    }
    
//MARK: - Generation
    fileprivate func generateNavController(vc: UIViewController, title: String, image: UIImage) -> UINavigationController{
        let navController = UINavigationController(rootViewController: vc)
        navController.title = title
        navController.tabBarItem.image = image
        return navController
    }
    
}
