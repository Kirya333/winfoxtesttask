//
//  ProductTableViewCell.swift
//  WinFox
//
//  Created by Кирилл Тарасов on 28.10.2021.
//

import UIKit
import SDWebImage

protocol ProductTableViewCellDelegate: AnyObject {
    func didAddTapButton(product: Product)
}

class ProductTableViewCell: UITableViewCell {
    
    //MARK: - Properties
    weak var delegate: ProductTableViewCellDelegate?
    static let identifier = "ProductTableViewCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var myImageView: UIImageView!
    
    private var product:Product?
    
    
    
    //MARK: - Configuration
    func configure(product: Product) {
        self.product = product
        nameLabel.text! = product.name
        priceLabel.text! = "Цена: " + String(product.price)
        weightLabel.text = "Рейтинг: " + String(product.weight)
        myImageView.sd_setImage(with: URL(string: product.image), completed: nil)
    }

    static func nib() -> UINib {
        return UINib(nibName: "ProductTableViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func addToCartButtonTapped(_ sender: Any) {
        delegate?.didAddTapButton(product: product!)
    }
    
    
}
