//
//  ProductsViewController.swift
//  WinFox
//
//  Created by Кирилл Тарасов on 28.10.2021.
//

import UIKit

class ProductsViewController: UIViewController {
    
//MARK: - Properties
    let realmService = RealmService.shared
    let networkService = NetworkService()
    var products = [Product]()
    
    @IBOutlet weak var productTableView: UITableView!

//MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.productTableView.register(ProductTableViewCell.nib(), forCellReuseIdentifier: ProductTableViewCell.identifier)
        self.productTableView.delegate = self
        self.productTableView.dataSource = self
        
        networkService.obtainPosts { [weak self] (result) in
            switch result {
            case .success(let products):
                self?.products = products
                DispatchQueue.main.async {
                    self?.productTableView.reloadData()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
    }
    
    // MARK: - Alerts
    private func showAlertAddToShoppingCart() {
        let alert = UIAlertController(title: "Товар добавлен в корзину", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ок", style: .cancel, handler: { action in
            
        }))
        present(alert, animated: true)
    }
    
    private func showAlertDidNotAddToShoppingCart() {
        let alert = UIAlertController(title: "Товар уже добавлен в корзину", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ок", style: .cancel, handler: { action in
            
        }))
        present(alert, animated: true)
    }
    
// MARK: - Product Sort
    @IBAction func sortByPriceTapped(_ sender: Any) {
        products.sort {
            $0.price < $1.price
        }
        productTableView.reloadData()
    }
    
    
    @IBAction func sortByWeightTapped(_ sender: Any) {
        products.sort {
            $0.weight < $1.weight
        }
        productTableView.reloadData()
    }
    

}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension ProductsViewController: UITableViewDelegate, UITableViewDataSource, ProductTableViewCellDelegate {
    func didAddTapButton(product: Product) {
        let realmProduct = ProductRealm(product: product)
        
        let products = Array(realmService.realm.objects(ProductRealm.self))
        
        for product in products {
            if product.id == realmProduct.id {
                showAlertDidNotAddToShoppingCart()
                return
            }
        }
        
        realmProduct.count += 1
        realmService.create(realmProduct)
        
        showAlertAddToShoppingCart()
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProductTableViewCell.identifier, for: indexPath) as! ProductTableViewCell
        let product = products[indexPath.row]
        
        cell.configure(product: product)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 228.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let product = products[indexPath.row]
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let infoProductVC = storyboard.instantiateViewController(identifier: "infoProductVC") as! InfoProductViewController
        infoProductVC.configure(product: product)
        
        self.navigationController?.pushViewController(infoProductVC, animated: true)

    }
}
