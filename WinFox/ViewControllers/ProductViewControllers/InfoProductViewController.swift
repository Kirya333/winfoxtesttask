//
//  InfoProductViewController.swift
//  WinFox
//
//  Created by Кирилл Тарасов on 28.10.2021.
//


import UIKit
import SDWebImage

class InfoProductViewController: UIViewController {
    
    //MARK: - Properties
    let realmService = RealmService.shared
    private var product: Product?
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var decsriptionLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var addToCartButton: UIButton!

    //MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let infoProduct = product
        else { return }
        
        photoImageView.sd_setImage(with: URL(string: infoProduct.image), completed: nil)
        nameLabel.text! = infoProduct.name
        priceLabel.text! = "Цена: " + String(infoProduct.price)
        decsriptionLabel.text! = infoProduct.desc.html2String
        weightLabel.text! = "Рейтинг: " + String(infoProduct.weight)
        
        if checkProductInCart(product: infoProduct) {
            addToCartButton.backgroundColor = .yellow
        }
    }
    
    //MARK: - Configuration
    func configure(product: Product) {
        self.product = product
    }
    

    //MARK: - Alerts
    private func showAlertAddToShoppingCart() {
        let alert = UIAlertController(title: "Товар добавлен в корзину", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ок", style: .cancel, handler: { action in
            
        }))
        present(alert, animated: true)
    }
    
    private func showAlertDidNotAddToShoppingCart() {
        let alert = UIAlertController(title: "Товар уже добавлен в корзину", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ок", style: .cancel, handler: { action in
            
        }))
        present(alert, animated: true)
    }
    
    private func checkProductInCart(product: Product) -> Bool {
        let realmProduct = ProductRealm(product: product)
        
        let products = Array(realmService.realm.objects(ProductRealm.self))
        
        for product in products {
            if product.id == realmProduct.id {
                
                return true
            }
        }
        return false
    }
    
    @IBAction func addToCartTapButton(_ sender: Any) {
        let realmProduct = ProductRealm(product: product!)
        
        let products = Array(realmService.realm.objects(ProductRealm.self))
        
        for product in products {
            if product.id == realmProduct.id {
                showAlertDidNotAddToShoppingCart()
                return
            }
        }
        
        realmProduct.count += 1
        realmService.create(realmProduct)
        
        addToCartButton.backgroundColor = .yellow
        showAlertAddToShoppingCart()
    }
    
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
