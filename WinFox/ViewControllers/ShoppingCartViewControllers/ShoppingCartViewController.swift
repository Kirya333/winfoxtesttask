//
//  ShoppingCartViewController.swift
//  WinFox
//
//  Created by Кирилл Тарасов on 28.10.2021.
//

import UIKit
import RealmSwift

class ShoppingCartViewController: UIViewController {
    
    //MARK: - Properties
    let realmService = RealmService.shared
    var notificationToken: NotificationToken?
    var products: Results<ProductRealm>
    
    var amointPrice = 0
    
    @IBOutlet weak var amountPriceLabel: UILabel!
    @IBOutlet weak var cartTableView: UITableView!
    
    //MARK: - Init
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)   {
        self.products = RealmService.shared.realm.objects(ProductRealm.self)
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        }


    required init?(coder aDecoder: NSCoder) {
        self.products = RealmService.shared.realm.objects(ProductRealm.self)
        super.init(coder: aDecoder)
        }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cartTableView.register(CartTableViewCell.nib(), forCellReuseIdentifier: CartTableViewCell.identifier)
        self.cartTableView.delegate = self
        self.cartTableView.dataSource = self
        
        notificationToken = products.observe { [weak self] (changes) in
            guard let tableView = self?.cartTableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
                self?.recalculateTheCost()
            case .update(_, let deletions, let insertions, let modifications):
                tableView.performBatchUpdates({
                    tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0) }),
                        with: .automatic)
                    tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                        with: .automatic)
                    tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                        with: .automatic)
                    self?.recalculateTheCost()
                })
            case .error(let error):
                fatalError("\(error)")
            }
        }

        
    }
    
    
    private func recalculateTheCost() {
        amointPrice = 0
        for product in products {
            amointPrice += product.price*product.count
        }
        self.amountPriceLabel.text! = "Общая стоимость: \(amointPrice)"
    }
    

}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension ShoppingCartViewController: UITableViewDelegate, UITableViewDataSource, CartTableViewCellDelegate {
    func didAddTapButton(product: ProductRealm) {
        realmService.update(product, with: ["count":product.count+1])
    }
    
    func didMinusTapButton(product: ProductRealm) {
        if (product.count > 1) {
            realmService.update(product, with: ["count":product.count-1])
        }
    }
    
    func didDeleteTapButton(product: ProductRealm) {
        realmService.delete(product)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CartTableViewCell.identifier, for: indexPath) as! CartTableViewCell

        let product = products[indexPath.row]
        
        cell.configure(product: product)
        cell.delegate = self
        return cell
    }
    
    
}
