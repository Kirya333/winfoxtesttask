//
//  CartTableViewCell.swift
//  WinFox
//
//  Created by Кирилл Тарасов on 28.10.2021.
//

import UIKit

protocol CartTableViewCellDelegate: AnyObject {
    func didAddTapButton(product: ProductRealm)
    func didMinusTapButton(product: ProductRealm)
    func didDeleteTapButton(product: ProductRealm)
}

class CartTableViewCell: UITableViewCell {
    
    //MARK: - Properties
    static let identifier = "CartTableViewCell"
    weak var delegate: CartTableViewCellDelegate?
    private var product: ProductRealm?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    static func nib() -> UINib {
        return UINib(nibName: "CartTableViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: - Configuration
    func configure(product: ProductRealm) {
        self.product = product
        nameLabel.text! = product.name
        countLabel.text! = "Количество: \(product.count)"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    @IBAction func didAddTapButton(_ sender: Any) {
        delegate?.didAddTapButton(product: product!)
    }
    
    
    @IBAction func didMinusTapButton(_ sender: Any) {
        delegate?.didMinusTapButton(product: product!)
    }
    
    @IBAction func didDeleteTapButton(_ sender: Any) {
        delegate?.didDeleteTapButton(product: product!)
    }
    
}
